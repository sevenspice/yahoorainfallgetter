# 概要

Yahoo気象情報APIとジオコーダーAPIを使用した時間毎の降水量を取得するライブラリ

* [気象情報API](http://developer.yahoo.co.jp/webapi/map/openlocalplatform/v1/weather.html)

* [ジオコーダーAPI](http://developer.yahoo.co.jp/webapi/map/openlocalplatform/v1/geocoder.html)

# ビルドに必要な環境

* [Gradle](https://gradle.org/) >= 2.9
* YahooデベロッパーネットワークのアプリケーションID
    * [Yahooデベロッパーネットワーク](http://developer.yahoo.co.jp/start/)

## ビルドとテスト

1. 好きなディレクトリに`git clone `でプロジェクトを取得
2. ターミナルからプロジェクトに移動
3. `cp src/test/java/resources/yahoo.properties.origin src/test/java/resources/yahoo.properties`でコピー
4. `vi src/test/java/resources/yahoo.properties`でファイルにAPIで使用するキーを追記して保存
5. `gradle build`でビルド
    * テストを行わなずビルドする場合は`gradle build -x test`
6. `build/libs/`ディレクトリ直下に`yahoo_weather_get.jar`が生成されていることを確認