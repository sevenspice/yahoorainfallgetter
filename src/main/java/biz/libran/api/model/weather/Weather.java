
package biz.libran.api.model.weather;

import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("Date")
    public String date;
    @SerializedName("Rainfall")
    public String rainfall;
    @SerializedName("Type")
    public String type;

}
