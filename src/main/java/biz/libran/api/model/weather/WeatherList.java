
package biz.libran.api.model.weather;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class WeatherList {

    @SerializedName("Weather")
    public List<Weather> weather;

}
