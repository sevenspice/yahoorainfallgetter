
package biz.libran.api.model.weather;

import com.google.gson.annotations.SerializedName;

public class Geometry {

    @SerializedName("Coordinates")
    public String coordinates;
    @SerializedName("Type")
    public String type;

}
