
package biz.libran.api.model.weather;

import com.google.gson.annotations.SerializedName;

public class Feature {

    public Geometry  Geometry;

    @SerializedName("Id")
    public String    id;
    @SerializedName("Name")
    public String    name;

    public Property   Property;

}