
package biz.libran.api.model.geo;

import com.google.gson.annotations.SerializedName;

public class ResultInfo {

    @SerializedName("Copyright")
    public String copyright;
    @SerializedName("Count")
    public String count;
    @SerializedName("Description")
    public String description;
    @SerializedName("Latency")
    public String latency;
    @SerializedName("Start")
    public String start;
    @SerializedName("Status")
    public String status;
    @SerializedName("Total")
    public String total;

}