
package biz.libran.api.model.geo;

import com.google.gson.annotations.SerializedName;

public class Feature {

    @SerializedName("Description")
    public String description;

    public Geometry Geometry;

    @SerializedName("Gid")
    public String gid;
    @SerializedName("Id")
    public String id;
    @SerializedName("Name")
    public String name;

    public Property Property;

}