package biz.libran.api.model.geo;

import com.google.gson.annotations.SerializedName;

public class Geometry {

    @SerializedName("BoundingBox")
    public String boundingBox;
    @SerializedName("Coordinates")
    public String coordinates;
    @SerializedName("Type")
    public String type;

}