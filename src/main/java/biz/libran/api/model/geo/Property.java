
package biz.libran.api.model.geo;

import com.google.gson.annotations.SerializedName;

public class Property {

    @SerializedName("Address")
    public String address;
    @SerializedName("AddressMatchingLevel")
    public String addressMatchingLevel;
    @SerializedName("AddressType")
    public String addressType;
    @SerializedName("CassetteId")
    public String cassetteId;

    public Country Country;

    @SerializedName("GovernmentCode")
    public String governmentCode;
    @SerializedName("Uid")
    public String uid;
    @SerializedName("Yomi")
    public String yomi;

}