
package biz.libran.api.model.geo;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Result {

    @SerializedName("Feature")
    public List<Feature> features;
    @SerializedName("ResultInfo")
    public ResultInfo resultInfo;

}
