
package biz.libran.api.model.geo;

import com.google.gson.annotations.SerializedName;

public class Country {

    @SerializedName("Code")
    public String code;
    @SerializedName("Name")
    public String name;

}
