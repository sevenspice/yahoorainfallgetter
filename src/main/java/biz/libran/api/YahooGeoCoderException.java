
package biz.libran.api;

/**
 * YahooGeoCoderクラスで使用する例外クラス
 */
public class YahooGeoCoderException extends Exception{
    private static final long serialVersionUID = 2L;
    public YahooGeoCoderException(String message){
        super(message);
    }
}