package biz.libran.api;

import biz.libran.http.Connection;
import java.util.logging.Logger;

/**
 * 気象情報APIへの接続を担うクラス
 */
public class YahooWeatherConnection extends Connection{
    private static final Logger logger = Logger.getLogger(YahooWeatherConnection.class.getName());

    // 固定値
    private static final String PROTOCOL = "GET";
    private static final String SCHEME   = "https";
    private static final String HOST     = "map.yahooapis.jp";
    private static final String PATH     = "/weather/V1/place";

    // メンバー
    private final String appId;

    public String getAppId(){
        return this.appId;
    }

    public YahooWeatherConnection(
        String appId
    ){

        super(PROTOCOL, SCHEME, HOST, PATH);
        this.appId = appId;

    }

}