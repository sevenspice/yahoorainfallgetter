
package biz.libran.api;

import biz.libran.http.Connection;
import java.util.logging.Logger;

/**
 * ジオコーダーAPIへのアクセスを担うクラス
 */
public class YahooGeoCoderConnection extends Connection{
    private static final Logger logger = Logger.getLogger(YahooWeatherConnection.class.getName());

    // 固定値
    private static final String PROTOCOL = "GET";
    private static final String SCHEME   = "https";
    private static final String HOST     = "map.yahooapis.jp";
    private static final String PATH     = "/geocode/V1/geoCoder";

    // メンバー
    private final String appId;

    public String getAppId(){
        return this.appId;
    }

    public YahooGeoCoderConnection(
        String appId
    ){

        super(PROTOCOL, SCHEME, HOST, PATH);
        this.appId = appId;

    }

}
