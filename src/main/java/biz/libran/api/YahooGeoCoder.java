package biz.libran.api;
import biz.libran.api.model.geo.Result;

import biz.libran.http.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
// import java.util.logging.Level;

/**
 * ジオコーダーAPIを使用するクラス
 */
public class YahooGeoCoder {
    private static final Logger logger = Logger.getLogger(YahooWeather.class.getName());

    // メンバ
    private final Connection connection;
    private String output = "json";
    private String al = "2";     // 住所検索レベル
    private String ar = "eq";    // 住所レベルの範囲
    private String ei = "UTF-8"; // 入力文字列の文字コードのエンコード形式

    public Connection getConnection(){
        return this.connection;
    }

    public String getOutput(){
        return this.output;
    }

    /**
     * 結果の取得データ構造
     * @param output json: JSON xml: XML
     */
    public void setOutput(String output){
        this.output = output;
    }

    public String getAl(){
        return this.al;
    }

    /**
     * 住所検索レベル
     * @param al 1:都道府県レベル 2:市区町村レベル 3:町、大字レベル 4:丁目、字レベル
     */
    public void setAl(String al){
        this.al = al;
    }

    public String getEi(){
        return this.ei;
    }

    /**
     * 入力文字列の文字コードのエンコード形式
     * @param ei UTF-8:UTF-8
     */
    public void setEi(String ei){
        this.ei = ei;
    }

    public String getAr(){
        return this.ar;
    }

    /**
     * 住所レベルの範囲
     * @param ar ge: 以上 le: 以下 eq:等しい
     */
    public void setAr(String ar){
        this.ar = ar;
    }

    public YahooGeoCoder(Connection connection){ this.connection = connection; }

    /**
     * 経度・緯度を取得する
     * @param query 検索する住所を指定
     * @return 取得した経度・緯度を返却. 取得できなければ空文字を返却する
     * @throws YahooGeoCoderException 座標情報の取得し失敗したら送出される
     */
    public String getCoordinates(String query) throws YahooGeoCoderException {

        String coordinates = "";
        if( query == null || query.isEmpty() ){ 
            throw new YahooGeoCoderException("Invalid argument.");
        }

        Gson gson = new Gson();
        Result result = null;
        // ダウンキャスト
        YahooGeoCoderConnection yahooGeoCoderConnection = (YahooGeoCoderConnection) this.getConnection();

        // クエリ
        Map<String, String> queries = new HashMap<String, String>();
        queries.put("appid",  yahooGeoCoderConnection.getAppId());
        queries.put("query",  query);
        queries.put("output", this.getOutput());
        queries.put("ei",     this.getEi());
        queries.put("al",     this.getAl());
        queries.put("ar",     this.getAr());

        yahooGeoCoderConnection.setQueries(queries);

        // 通信
        List<String> response = yahooGeoCoderConnection.execute();

        if(response.size() > 0 && response.get(0).equals("200")){

            // JSONパース
            try{

                result = gson.fromJson(response.get(1), Result.class);
                // APIからエラーが返却された場合
                if(result == null || result.resultInfo == null || result.features == null){
                    throw new YahooGeoCoderException(response.get(1));
                }

            }catch(JsonSyntaxException e){
                throw new YahooGeoCoderException("Failure in parsing JSON.");
            }
        }

        // 位置情報の取得
        if(!result.features.isEmpty() && result.features.get(0) != null){
            coordinates = result.features.get(0).Geometry.coordinates;
        }else{
            throw new YahooGeoCoderException("Coordinate information does not exist.");
        }

        return coordinates;

    }

}
