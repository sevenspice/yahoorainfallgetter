package biz.libran.api;
import biz.libran.api.model.weather.Result;
import biz.libran.api.model.weather.Feature;
import biz.libran.api.model.weather.Weather;

import biz.libran.http.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.util.TimeZone;

/**
 * 気象情報APIを使用するクラス
 */
public class YahooWeather{
    private static final Logger logger = Logger.getLogger(YahooWeather.class.getName());

    // メンバ
    private final Connection connection;
    private String output   = "json";
    private String timezone = "Asia/Tokyo";
    private int past     = 0;
    private int interval = 10;

    public Connection getConnection(){
        return this.connection;
    }

    public String getOutput(){
        return this.output;
    }

    /**
     * 結果の取得データ構造
     * @param output json: JSON xml: XML
     */
    public void setOutput(String output){
        this.output = output;
    }

    public int getPast(){
        return this.past;
    }

    /**
     * 過去の降水強度の実測値の取得を指定
     * @param past 0:取得しない 1:1時間前までを取得 2:2時間前までを取得
     */
    public void setPast(int past){
        this.past = past;
    }

    public int getInterval(){
        return this.interval;
    }

    /**
     * 取得間隔
     * @param interval 10:10分毎 5:5分毎
     */
    public void setInterval(int interval){
        this.interval = interval;
    }

    public String getTimeZone(){
        return this.timezone;
    }

    public void setTimezone(String timezone){
        this.timezone = timezone;
    }

    /**
     * コンストラクタ
     * @param connection APIへの接続クラスを指定
     */
    public YahooWeather(Connection connection){ this.connection = connection; }

    /**
     * 気象情報を複数地点同時に取得する
     * @param coordinates 複数地点の経度・緯度情報を格納したリストを指定
     * @param date 取得日時を指定（現在から2時間前までを指定可能）
     * @return 気象情報を格納したリストを返却する.
     *         存在しない地点の場合, 降水量のデータに null が格納される
     * @throws YahooWeatherException 気象情報の取得に失敗で送出される
     */
    public Map<String, List<Map<String, String>>> getWeathers(List<String> coordinates, Date date) throws YahooWeatherException {

        Gson gson = new Gson();
        Result result = null;
        String query  = "";
        Map<String, List<Map<String, String>>> weathers = new HashMap<String, List<Map<String, String>>>();
        // ダウンキャスト
        YahooWeatherConnection yahooWeatherConnection = (YahooWeatherConnection) this.getConnection();

        for(String c : coordinates){

            if(!this.isCoordinates(c)){
                throw new YahooWeatherException("Specification format of coordinates will be different.");
            }

            query += c + " ";

        }
        // 末尾のスペースを削除
        query = query.substring(0, (query.length() - 1));

        yahooWeatherConnection.setQueries(this.generateQueries(yahooWeatherConnection, query, date));
        // 通信
        List<String> response = yahooWeatherConnection.execute();

        if(response.size() > 0 && response.get(0).equals("200")){

            //JSONパース
            try{

                result = gson.fromJson(response.get(1), Result.class);
                // APIからエラーが返却された場合
                if(result == null || result.resultInfo == null || result.features == null){
                    throw new YahooWeatherException(response.get(1));
                }

            }catch(JsonSyntaxException e){
                throw new YahooWeatherException(e.toString());
            }

        }else{

            if(response.size() > 0){
                throw new YahooWeatherException(response.get(1));
            }else{
                throw new YahooWeatherException("Unknown communication failure.");
            }
        }


        if(result.features != null){

            for(Feature feature : result.features){

                List<Map<String, String>> w = new ArrayList<Map<String, String>>();

                for(Weather weather: feature.Property.WeatherList.weather){

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("date", weather.date);
                    map.put("rainfall", weather.rainfall);
                    map.put("type", weather.type);
                    // リストに格納
                    w.add(map);

                }

                weathers.put(feature.Geometry.coordinates, w);

            }

        }else{
            throw new YahooWeatherException("Features data is null");
        }

        return weathers;

    }

    /**
     * 気象情報の取得
     * @param coordinates 軽度・緯度を文字列で指定
     * @return 気象情報を格納したリストを返却する.
     * @throws YahooWeatherException 気象情報の取得に失敗で送出される
     */
    public List<Map<String, String>> getWeather(String coordinates) throws YahooWeatherException {
        return this.getWeather(coordinates, null);
    }

    /**
     * 気象情報の取得
     * @param coordinates 経度・緯度を文字列で指定
     * @param date        取得日時を指定（現在から2時間前までを指定可能）
     * @return 気象情報を格納したリストを返却する.
     *         存在しない地点の場合, 降水量のデータに null が格納される
     * @throws YahooWeatherException 気象情報の取得に失敗で送出される
     */
    public List<Map<String, String>> getWeather(String coordinates, Date date) throws YahooWeatherException {

        Gson gson = new Gson();
        Result result = null;
        List<Map<String, String>> weathers = new ArrayList<Map<String, String>>();
        // ダウンキャスト
        YahooWeatherConnection yahooWeatherConnection = (YahooWeatherConnection) this.getConnection();

        if(!this.isCoordinates(coordinates)){
            throw new YahooWeatherException("Specification format of coordinates will be different.");
        }

        yahooWeatherConnection.setQueries(this.generateQueries(yahooWeatherConnection, coordinates, date));
        // 通信
        List<String> response = yahooWeatherConnection.execute();

        if(response.size() > 0 && response.get(0).equals("200")){

            // JSONパース
            try{

                result = gson.fromJson(response.get(1), Result.class);
                // APIからエラーが返却された場合
                if(result == null || result.resultInfo == null || result.features == null){
                    throw new YahooWeatherException(response.get(1));
                }

            }catch(JsonSyntaxException e){
                throw new YahooWeatherException(e.toString());
            }

        }

        if(!result.features.isEmpty() && result.features.get(0) != null){

            for(Weather weather : result.features.get(0).Property.WeatherList.weather){

                Map<String, String> map = new HashMap<String, String>();
                map.put("date", weather.date);
                map.put("rainfall", weather.rainfall);
                map.put("type", weather.type);
                // リストに格納
                weathers.add(map);

            }

        }else{
            throw new YahooWeatherException("Features data is nothing");
        }

        return weathers;

    }

    /**
     * 通信時に付加するクエリを生成する
     * @param connection YahooWeatherConnection クラスのインスタンスを指定
     * @param coodinates 経度・緯度の文字列を指定
     * @param date       取得日を指定
     * @return 生成されたハッシュマップを返却
     */
    private Map<String, String> generateQueries(YahooWeatherConnection connection, String coordinates, Date date){

        // クエリ
        Map<String, String> queries = new HashMap<String, String>();
        queries.put("appid",       connection.getAppId());
        queries.put("coordinates", coordinates);
        queries.put("output",      this.getOutput());
        queries.put("past",        String.valueOf(this.getPast()));
        queries.put("interval",    String.valueOf(this.getInterval()));

        Date toDay = new Date();
        // タイムゾーンの取得
        TimeZone tz = TimeZone.getTimeZone(this.getTimeZone());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        formatter.setTimeZone(tz);

        if(date == null){

            // 現在時刻をセット
            queries.put("date", formatter.format(toDay));

        }else{
            queries.put("date", this.isWithinTwoHours(date));
        }

        return queries;

    }

    /**
     * 引数に指定された時間が現在時刻から2時間以内ならば文字列にしてそのまま返却する
     * 2時間より前ならば2時間前の文字列にして返却する
     * @param date 確認したい日時を指定
     * @return YYYYMMDDHHMI形式で返却
     */
    private String isWithinTwoHours(Date date){

        // タイムゾーンの取得
        TimeZone tz = TimeZone.getTimeZone(this.getTimeZone());
        // フォーマッター
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        formatter.setTimeZone(tz);

        // 現在時刻の取得
        Date toDay = new Date();
        // 現在時刻から2時間前
        Date b2hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 2));

        // 比較
        int diff = date.compareTo(b2hDate);
        int diffToDay = date.compareTo(toDay);

        if(diff == 0){
            return formatter.format(date);
        }else if(diff > 0){
            // 2時間前未満の場合

            if(diffToDay == 0){
                return formatter.format(date);
            }else if(diffToDay > 0){
                // 未来の時間は返さず現在時刻を返却
                return formatter.format(toDay);
            }else if(diffToDay < 0){
                return formatter.format(date);
            }else{
                // ここは通らない
                return "";
            }

        }else if(diff < 0){
            return formatter.format(b2hDate);
        }else{
            // ここは通らない
            return "";
        }

    }

    /**
     * 位置情報の書式が正しい場合にTRUEを返却する
     * @param coordinates 経度・緯度を文字列で指定
     * @return 書式が正しければTRUE, 異なる場合はFALSEを返却
     */
    private boolean isCoordinates(String coordinates){

        String coordinatesPattern = "^-?[0-9]+.[0-9]+,-?[0-9]+.[0-9]+";
        Pattern pattern = Pattern.compile(coordinatesPattern);
        Matcher matcher = pattern.matcher(coordinates);

        return matcher.matches();

    }

}