
package biz.libran.api;

/**
 * YahooWeatherクラスで使用する例外クラス
 */
public class YahooWeatherException extends Exception{
    private static final long serialVersionUID = 1L;
    public YahooWeatherException(String message){
        super(message);
    }
}
