package biz.libran.api;

import org.junit.Test;
import org.junit.Before;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class YahooWeatherTest {
    private static final Logger logger = Logger.getLogger(YahooWeatherTest.class.getName());

    private String appId;


    @Before
    public void initialize(){

        ResourceBundle bundle = ResourceBundle.getBundle("yahoo");
        this.appId = bundle.getString("APP_ID");

    }

    @Test
    /**
     * インスタンスの生成をテスト
     */
    public void generateYahooWeather(){

        YahooWeather yahooWeather = this.generateInstance();
        // デフォルト値のテスト
        assertThat("json",       is(yahooWeather.getOutput()));
        assertThat("Asia/Tokyo", is(yahooWeather.getTimeZone()));
        assertThat(0,            is(yahooWeather.getPast()));
        assertThat(10,           is(yahooWeather.getInterval()));

    }

    @Test
    public void getWeathers(){

        // APIにアクセスするため必要に応じてコメントを外してテストを行うこと
        /*
        List<String> coordinates = new ArrayList<String>();
        coordinates.add("133.04838210,35.46806320");
        coordinates.add("133.25092230,35.43144600");
        coordinates.add("132.75468850,35.36703840");

        // 正常系
        YahooWeather yahooWeather = this.generateInstance();
        Map<String, List<Map<String, String>>> weathers;
        try{
            weathers = yahooWeather.getWeathers(coordinates, new Date());
            // マップが空でないこと
            assertThat(3, is(weathers.size()));
        }catch(YahooWeatherException e){
            assertThat(true, is(false));
        }

        // 正常系
        try{
            // 経度・緯度の書式が違うものが含まれる
            coordinates.add("132.75468850.35.36703840");
            weathers = yahooWeather.getWeathers(coordinates, new Date());
            // マップが空であること
            assertThat(true, is(false));
        }catch(YahooWeatherException e){
            // 例外が送出されること
            assertThat(true, is(true));
        }
        */

    }

    @Test
    public void getWeatherArgumentCoordinates(){

        // APIにアクセスするため必要に応じてコメントを外してテストを行うこと
        /*
        String coordinates = "133.04838210,35.46806320"; // 松江
        YahooWeather yahooWeather = this.generateInstance();

        // 正常系
        // 通常取得
        try{

        List<Map<String, String>> weathers = yahooWeather.getWeather(coordinates);
        // リストが空でないこと
        assertThat(weathers, not(empty()));

        }catch(YahooWeatherException e){
            assertThat(true, is(false));
        }
        */

    }

    @Test
    public void getWeather(){

        // APIにアクセスするため必要に応じてコメントを外してテストを行うこと
        /*
        String coordinates = "132.9605103,35.46736"; // 松江
        Date toDay = new Date();
        YahooWeather yahooWeather = this.generateInstance();
         List<Map<String, String>> weathers = null;

        // 正常系
        // 通常取得
        try{

            weathers = yahooWeather.getWeather(coordinates, toDay);
            // リストが空でないこと
            assertThat(weathers, not(empty()));

        }catch(YahooWeatherException e){
            assertThat(true, is(false));
        }

        // 正常系
        // 位置情報の書式が間違い
        try{

            weathers = yahooWeather.getWeather("132.9605103.35.46736", toDay);
            // リストが空であること
            // assertThat(weathers, is(empty()));
            assertThat(true, is(false));

        }catch(YahooWeatherException e){
            assertThat(true, is(true));
        }

        // 正常系
        // 日時がNULL
        try{

            weathers = yahooWeather.getWeather("132.9605103,35.46736", null);
            // リストが空でないこと
            assertThat(weathers, not(empty()));

        }catch(YahooWeatherException e){
            assertThat(true, is(false));
        }

        // 正常系
        // 経度・緯度が逆
        try{

            weathers = yahooWeather.getWeather("35.46736,132.9605103", toDay);
            // 降水量が"null"であること
            assertThat(null, is(weathers.get(0).get("rainfall")));

        }catch(YahooWeatherException e){
            assertThat(true, is(false));
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat testFormatter = new SimpleDateFormat("yyyyMMddHH");

        try{

            // 正常系
            // 1時間前を指定
            Date b1hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 1));
            try{

                weathers = yahooWeather.getWeather("132.9605103,35.46736", b1hDate);
                assertThat(weathers, not(empty()));
                // 1時間前のデータが取得されていること
                assertThat(testFormatter.format(b1hDate), is(testFormatter.format(formatter.parse(weathers.get(0).get("date")))));

            }catch(YahooWeatherException e){
                assertThat(true, is(false));
            }

            // 正常系
            // 2時間前を指定
            Date b2hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 2));
            try{

                 weathers = yahooWeather.getWeather("132.9605103,35.46736", b2hDate);
                 assertThat(weathers, not(empty()));
                 // 2時間前のデータが取得されていること
                 assertThat(testFormatter.format(b2hDate), is(testFormatter.format(formatter.parse(weathers.get(0).get("date")))));

            }catch(YahooWeatherException e){
                assertThat(true, is(false));
            }

            // 正常系
            // 3時間前を指定
            Date b3hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 3));
            try{

                weathers = yahooWeather.getWeather("132.9605103,35.46736", b3hDate);
                assertThat(weathers, not(empty()));
                // 2時間前のデータが取得されていること
                assertThat(testFormatter.format(b2hDate), is(testFormatter.format(formatter.parse(weathers.get(0).get("date")))));

            }catch(YahooWeatherException e){
                assertThat(true, is(false));
            }

            // 正常系
            // 1時間後を指定
            Date a1hDate = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 1));
            try{

                weathers = yahooWeather.getWeather("132.9605103,35.46736", a1hDate);
                assertThat(weathers, not(empty()));
                // 現在時刻のデータが取得されていること
                assertThat(testFormatter.format(toDay), is(testFormatter.format(formatter.parse(weathers.get(0).get("date")))));

            }catch(YahooWeatherException e){
                assertThat(true, is(false));
            }

        }catch(ParseException e){
            assertThat(true, is(false));
        }
        */

    }

    @Test
    public void generateQueries()
        throws NoSuchMethodException,
               SecurityException,
               IllegalAccessException,
               IllegalArgumentException,
               InvocationTargetException{

        String coordinates = "133.04838210,35.46806320"; // 松江
        YahooWeather yahooWeather = this.generateInstance();
        Method generateQueries = YahooWeather.class.getDeclaredMethod(
            "generateQueries", YahooWeatherConnection.class, String.class, Date.class);
        generateQueries.setAccessible(true);

        Map<String, String> query = this.autoCast(
            generateQueries.invoke(yahooWeather, new YahooWeatherConnection(this.appId), coordinates, new Date()));

        // 必要なキーが存在すること
        assertThat(query, hasKey("appid"));
        assertThat(query, hasKey("coordinates"));
        assertThat(query, hasKey("output"));
        assertThat(query, hasKey("past"));
        assertThat(query, hasKey("interval"));
        assertThat(query, hasKey("date"));

    }

    @Test
    public void isWithinTwoHours()
        throws NoSuchMethodException,
               SecurityException,
               IllegalAccessException,
               IllegalArgumentException,
               InvocationTargetException{

        YahooWeather yahooWeather = this.generateInstance();
        Method isWithinTwoHours = YahooWeather.class.getDeclaredMethod("isWithinTwoHours", Date.class);
        isWithinTwoHours.setAccessible(true);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");

        // 正常系
        // 現在時刻を指定する
        Date toDay = new Date();
        String result = (String) isWithinTwoHours.invoke(yahooWeather, toDay);
        // 現在時刻が返却される
        assertThat(formatter.format(toDay), is(result));

        // 現在時刻の1時間前を指定する
        Date b1hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 1));
        result = (String) isWithinTwoHours.invoke(yahooWeather, b1hDate);
        // 引数に与えた時間がそのまま返却される
        assertThat(formatter.format(b1hDate), is(result));

        // 正常系
        // 現在時刻の2時間前を指定する
        Date b2hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 2));
        result = (String) isWithinTwoHours.invoke(yahooWeather, b2hDate);
        // 引数に与えた時間がそのまま返却される
        assertThat(formatter.format(b2hDate), is(result));

        // 正常系
        // 現在時刻の3時間前を指定する
        Date b3hDate = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 3));
        result = (String) isWithinTwoHours.invoke(yahooWeather, b3hDate);
        // 引数に与えた時間ではなく現在時刻から2時間前の時間が返却される
        assertThat(formatter.format(b2hDate), is(result));

        // 正常系
        // 現在時刻から1時間後を指定する
        Date a1hDate = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 1));
        result = (String) isWithinTwoHours.invoke(yahooWeather, a1hDate);
        // 現在時刻が返却される
        assertThat(formatter.format(toDay), is(result));

        // 異常系
        // NULLを指定
        try{

            isWithinTwoHours.invoke(yahooWeather, (Object) null);
            assertThat(true, is(false));

        }catch(InvocationTargetException e){
            // 例外が発生すること
            assertThat(true, is(true));
        }

    }

    @Test
    public void isCoordinates()
        throws NoSuchMethodException,
               SecurityException,
               IllegalAccessException,
               IllegalArgumentException,
               InvocationTargetException{

        YahooWeather yahooWeather = this.generateInstance();
        Method isCoordinates = YahooWeather.class.getDeclaredMethod("isCoordinates", String.class);
        isCoordinates.setAccessible(true);

        // 正常系
        String coordinates = "132.9605103,35.46736"; // 松江
        boolean result = (boolean) isCoordinates.invoke(yahooWeather, coordinates);
        // true が返却されること
        assertThat(true, is(result));

        // 正常系
        // 区切りがピリオド
        coordinates = "132.9605103.35.46736";
        result = (boolean) isCoordinates.invoke(yahooWeather, coordinates);
        // false が返却されること
        assertThat(false, is(result));

        // 正常系
        // 文字が含まれる
        coordinates = "132.9605103x.35.46736y";
        result = (boolean) isCoordinates.invoke(yahooWeather, coordinates);
        // false が返却されること
        assertThat(false, is(result));

        // 正常系
        // 空文字
        coordinates = "";
        result = (boolean) isCoordinates.invoke(yahooWeather, coordinates);
        // false が返却されること
        assertThat(false, is(result));

        // 異常系
        // NULL
        coordinates = null;
        try{

            isCoordinates.invoke(yahooWeather, coordinates);
            assertThat(true, is(false));

        }catch(InvocationTargetException e){

            // 例外が発生すること
            assertThat(true, is(true));

        }

    }

    /**
     * テスト用のインスタンスを生成する
     */
    private YahooWeather generateInstance(){
        
        return new YahooWeather(new YahooWeatherConnection(this.appId));
    }

    @SuppressWarnings("unchecked")
    /**
     * キャストメソッド
     */
    private <T> T autoCast(Object object){

        T castObject = (T) object;
        return castObject;

    }

}