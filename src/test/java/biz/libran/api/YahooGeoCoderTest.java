
package biz.libran.api;

import org.junit.Test;
import org.junit.Before;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
// import java.lang.reflect.Method;
// import java.lang.reflect.InvocationTargetException;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YahooGeoCoderTest {
    private static final Logger logger = Logger.getLogger(YahooWeatherTest.class.getName());

    private String appId;

    @Before
    public void initialize(){

        ResourceBundle bundle = ResourceBundle.getBundle("yahoo");
        this.appId = bundle.getString("APP_ID");

    }

    @Test
    /**
     * インスタンスの生成をテスト
     */
    public void generateYahooGeoCoder(){

       YahooGeoCoder yahooGeoCoder = this.generateInstance();
        // デフォルト値のテスト
        assertThat("json",  is(yahooGeoCoder.getOutput()));
        assertThat("2",     is(yahooGeoCoder.getAl()));
        assertThat("eq",    is(yahooGeoCoder.getAr()));
        assertThat("UTF-8", is(yahooGeoCoder.getEi()));

    }

    @Test
    public void getCoordinates(){

        // APIにアクセスするため必要に応じてコメントを外してテストを行うこと
        /*
        String coordinatesPattern = "^-?[0-9]+.[0-9]+,-?[0-9]+.[0-9]+";
        Pattern pattern = Pattern.compile(coordinatesPattern);

        YahooGeoCoder yahooGeoCoder = this.generateInstance();
        String coordinates = "";

        // 正常系
        try{

            coordinates = yahooGeoCoder.getCoordinates("島根県松江市");
            Matcher matcher = pattern.matcher(coordinates);
            // 返却文字列が経度・緯度の書式にマッチすること
            assertThat(true, is(matcher.matches()));

        }catch(YahooGeoCoderException e){
            assertThat(true, is(false));
        }

        // 正常系
        // 存在しない住所を指定
        try{

            coordinates = yahooGeoCoder.getCoordinates("あぼかど");
            // 空文字が返却されること
            // assertThat("", is(coordinates));
            assertThat(true, is(false));

        }catch(YahooGeoCoderException e){
            assertThat(true, is(true));
        }

        // 正常系
        // 空文字を指定
        try{

            coordinates = yahooGeoCoder.getCoordinates("");
            // 空文字が返却されること
            // assertThat("", is(coordinates));
            assertThat(true, is(false));

        }catch(YahooGeoCoderException e){
            assertThat(true, is(true));
        }

        // 正常系
        // NULLを指定
        try{

            coordinates = yahooGeoCoder.getCoordinates(null);
            // 空文字が返却されること
            // assertThat("", is(coordinates));
            assertThat(true, is(false));

        }catch(YahooGeoCoderException e){
            assertThat(true, is(true));
        }
        */

    }

    /**
     * テスト用のインスタンスを生成する
     */
    private YahooGeoCoder generateInstance(){
        return new YahooGeoCoder(new YahooGeoCoderConnection(this.appId));
    }
    
}
