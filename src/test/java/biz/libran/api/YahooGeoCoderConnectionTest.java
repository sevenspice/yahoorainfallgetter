
package biz.libran.api;

import org.junit.Test;
import org.junit.Before;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class YahooGeoCoderConnectionTest {
    private static final Logger logger = Logger.getLogger(YahooWeatherTest.class.getName());

    private String appId;

    @Before
    public void initialize(){

        ResourceBundle bundle = ResourceBundle.getBundle("yahoo");
        this.appId = bundle.getString("APP_ID");

    }

    @Test
    /**
     * インスタンスの生成をテスト
     */
    public void generateYahooGeoCoderConnection(){

        YahooGeoCoderConnection connection = new YahooGeoCoderConnection(this.appId);
        assertThat("GET", is(connection.getProtocol()));
        assertThat("https", is(connection.getScheme()));
        assertThat("map.yahooapis.jp", is(connection.getHost()));
        assertThat("/geocode/V1/geoCoder", is(connection.getPath()));

    }
    
}
